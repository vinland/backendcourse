﻿using System;
using System.Threading.Tasks;

namespace CityBike {
    class Program {
        static void Main(string[] args) {
            bool _isOnline = true; // Flag to track if we are using realtime or not (default: is on)
            int _bikeCount = 0; // Bike count at the station, start from 0.
            ICityBikeDataFetcher _dataFetcher; // station datafetcher, uses ICityBikeDataFetcher interface.

            // IF there are arguments, check if second argument is "realtime" or "offline", change flag accordingly. Otherwise continue.
            if (args.Length >= 2) {
                if (args[1] == "realtime") {
                    _isOnline = true;
                } else if (args[1] == "offline") {
                    _isOnline = false;
                }
            } else if (args.Length == 0) {
                return;
            }
            //Write first argument (given bike station name) to console. 
           Console.WriteLine("Checking bike count at: '" + args[0]+ "'");
            
            // IF online flag is true, use the realtime, otherwise use offline mode.
            if (_isOnline) {
                _dataFetcher = new RealTimeCityBikeDataFetcher();
            } else {
                _dataFetcher = new OfflineCityBikeDataFetcher();
            }

            // Call for asyncronous call for bikecount with given argument (station name).
            var _task = _dataFetcher.GetBikeCountInStation(args[0]);
            // Make the call wait for result.
            _task.Wait();
            // Get bikecount from the result.
            _bikeCount = _task.Result;
            // IF given argument was not invalid (count is 999), do NOT print bikecount.
            if(_bikeCount != 999)
                Console.WriteLine("Bikes available at the station: " + _bikeCount.ToString());

        }
    }
}