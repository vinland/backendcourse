using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CityBike {
    public class RealTimeCityBikeDataFetcher : ICityBikeDataFetcher {
        static HttpClient client = new HttpClient();

        public async Task<int> GetBikeCountInStation(string stationName) {
            // Check that there are no digits in the given station name.
            try {
                foreach (var character in stationName.ToCharArray()) {
                    if (Char.IsDigit(character)) {
                        throw new ArgumentException();
                    }
                }
                // Get data from digitransit api.
                HttpResponseMessage _httpMessage = await client.GetAsync("http://api.digitransit.fi/routing/v1/routers/hsl/bike_rental");
                // Convert data into string
                string _messageText = System.Text.Encoding.UTF8.GetString(_httpMessage.Content.ReadAsByteArrayAsync().Result);
                // Create a readable stationlist from the data.
                BikeRentalStationList _stationList = (BikeRentalStationList) JsonConvert.DeserializeObject<BikeRentalStationList>(_messageText);

                int _bikeCount = 0; // Keep track of how many bikes are at the station
                bool _stationFound = false; // Flag to track if given station is found from the list.

                // Check every station in the station list data, if given station found, get bike count.
                foreach (var station in _stationList.stations) {
                    if (station.name == stationName) {
                        _bikeCount = station.bikesAvailable;
                        _stationFound = true;
                        break;
                    }
                }
                
                // IF given station name is not found, throw exception.
                if (!_stationFound) {
                    throw new NotFoundException();
                }

                return _bikeCount;
            
            // Catch execptions
            } catch (ArgumentException ex) {
                Console.WriteLine("Invalid argument: " + ex.Message);
                return 999;
            } catch (NotFoundException ex) {
                Console.WriteLine("'"+stationName+"'"+ ex.Message);
                return 999;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return 999;
            }
        }
    }
}