using System;

namespace CityBike {
    public class NotFoundException : Exception {
        public NotFoundException() : base(" station not found. Try again."){           
        }
        public NotFoundException(string message) : base(message) { }

        public NotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}