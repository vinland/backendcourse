using System;
using System.Threading.Tasks;

namespace CityBike {
    public class OfflineCityBikeDataFetcher : ICityBikeDataFetcher {
        public async Task<int> GetBikeCountInStation(string stationName) {              
            
            try {
                string[] lines = await System.IO.File.ReadAllLinesAsync(@"C:\Taustajarjestelmat\CityBike\bikedata.txt");

                int _bikeCount = 0; // Keep track of how many bikes are at the station
                bool _stationFound = false; // Flag to track if given station is found from the list.
                // Check that there are no digits in the given station name.
                foreach (var character in stationName.ToCharArray()) {
                    if (Char.IsDigit(character)) {
                        throw new ArgumentException();
                    }
                }
                
                foreach (string line in lines) {
                    // Read the line and split it in two at " : " separator, file is in format: (Station : Bikecount)
                    string[] subStrings = line.Split(" : ");
                    // Compare the station list name to given station name, if found, get bike count and break out of the loop.
                    if (subStrings.Length >= 2 && subStrings[0] == stationName) {
                        _stationFound = true;
                        _bikeCount = Int32.Parse(subStrings[1]); // parse the string into int.
                        break;
                    }
                }

                // IF given station name is not found, throw exception.
                if (!_stationFound) {
                    throw new NotFoundException();
                }
                return _bikeCount;
            // Catch execptions
            } catch (ArgumentException ex) {
                Console.WriteLine("Invalid argument: " + ex.Message);
                return 999;
            } catch (NotFoundException ex) {
                Console.WriteLine("'"+stationName+"'"+ ex.Message);
                return 999;
            } catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return 999;
            }

        }
    }
}